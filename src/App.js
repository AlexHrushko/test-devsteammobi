// @flow weak

import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import ButtonBase from 'material-ui/ButtonBase';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';
import Button from 'material-ui/Button';

const styles = theme => ({
  paper: {
    padding: 40,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    minHeight: 54,
    flexBasis: 0,
    flexGrow: 1,
    maxWidth: '100%',
    margin: 12,
  },
  root: {
    marginTop: theme.spacing.unit * 4,
    display: 'flex',
    flexWrap: 'wrap',
    minWidth: 300,
    width: '100%',
  },
  container: {
    flexGrow: 1,
    paddingTop: 30,
    paddingLeft: 200,
    paddingRight: 200,
    display: 'flex',
    flexWrap: 'wrap',
    position: 'relative',
  },
  image: {
    position: 'relative',
    height: 200,
    [theme.breakpoints.down('sm')]: {
      width: '100% !important', // Overrides inline-style
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme.spacing.unit + 6}px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
});

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      gallery: {
        photos: []
      },
      chooseImage: false,
      src: "",
      title: "",
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.renderChooseImage = this.renderChooseImage.bind(this);

  }

  componentDidMount() {
    axios({
      method: 'get',
      url: 'https://api.500px.com/v1/photos?feature=popular&consumer_key=wB4ozJxTijCwNuggJvPGtBGCRqaZVcF6jsrzUadF',
      responseType: 'json'
    }).then(response => {
      this.setState({
        gallery: response.data,
      });
    });
  }

  onSubmit() {
    this.setState({
      chooseImage: true
    });
  }

  handleRequestClose() {
    this.setState({ chooseImage: false });
  };

  handleRequestOpen(id) {
    const findImage = this.state.gallery.photos.find(photo => photo.id === id)
    this.setState({
      chooseImage: true,
      src: findImage.image_url,
      title: findImage.name
    });
  };

  renderChooseImage() {
    return (
      <div>
        <Dialog open={this.state.chooseImage}
          transition={Slide}
          onRequestClose={this.handleRequestClose}>
          <DialogTitle>
            {this.state.title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <img src={this.state.src}
                alt={this.state.title}
                style={{
                  width: 500,
                  height: 400
                }} />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequestClose} color="primary">
              Cancel
          </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

  render() {
    const classes = this.props.classes;
    return (
      <div className={classes.container}>
        <Paper className={classes.paper}>
          <Typography type="display1" align="left" gutterBottom>
            Photo
          </Typography>

          <div className={classes.root}>
            {this.state.gallery.photos.map(photo =>
              <ButtonBase
                focusRipple
                key={photo.id}
                onClick={this.handleRequestOpen.bind(this, photo.id)}
                className={classes.image}
                style={{
                  width: 150,
                }}
              >
                <div
                  className={classes.imageSrc}
                  style={{
                    backgroundImage: `url(${photo.image_url})`,
                  }}
                />
                <div className={classes.imageBackdrop} />
                <div className={classes.imageButton}>
                  <Typography
                    component="h3"
                    type="subheading"
                    color="inherit"
                    className={classes.imageTitle}
                  >
                    {photo.name}
                    <div className={classes.imageMarked} />
                  </Typography>
                </div>
              </ButtonBase>,
            )}
          </div>
        </Paper>
        <div>{this.renderChooseImage()}</div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
